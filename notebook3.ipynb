{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook 3 - Coupling one particle to one mode\n",
    "\n",
    "Let us consider the long-wavelength dipole approximation of the Pauli-Fierz Hamiltonian, i.e., we replace $\\hat{\\mathbf{A}}_{\\perp}(\\mathbf{r}) \\rightarrow \\hat{\\mathbf{A}}_{\\perp}(0) = \\hat{\\mathbf{A}}_{\\perp}$. This approximation will be discussed in more detail in the following in this lecture. We further just consider only a single (one-dimensional in agreement with Notebook 2) particle and the vector potential consists of only one photon mode (similar to Notebook 1). The corresponding Hamiltonian is then\n",
    "\n",
    "$$\n",
    "\\hat{H}'_{\\text{vel}} = \\frac{1}{2m} \\left(- \\mathrm{i} \\hbar \\partial_x + \\frac{|e|}{c} \\hat{A} \\right)^2 + \\hbar \\omega \\left(\\hat{a}^\\dagger \\hat{a} + \\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "with the single-mode vector potential \n",
    "\n",
    "$$\n",
    "\\hat{A} = \\sqrt{\\frac{\\hbar c^2}{\\epsilon_0 L^3}} \\frac{1}{\\sqrt{2 \\omega}} (\\hat{a} + \\hat{a}^\\dagger)\n",
    "$$\n",
    "\n",
    "on $L^2(\\Omega_{\\text{spatial}}) \\otimes L^2(\\mathbb{R})$ with $\\Omega_{\\text{spatial}} = [0,L]$ and periodic boundary conditions for the matter subsystem. We note that a more precise notation for the operators on the coupled Hilbert space would be\n",
    "\n",
    "$$\n",
    "-\\mathrm{i} \\hbar \\partial_x \\otimes \\hat{\\mathbb{1}} \\quad \\text{and} \\quad \\hat{\\mathbb{1}}_{\\text{spatial}} \\otimes \\hat{A},\n",
    "$$\n",
    "\n",
    "and accordingly for the other operators in $\\hat{H}'_{\\text{vel}}$. But we will follow the usual convention and suppress the identity operators throughout the school."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Q. 3.1\n",
    "\n",
    "Diagonalize the Hamiltonian $\\hat{H}'_{\\text{vel}}$. \n",
    "\n",
    "<details>\n",
    "<summary>Hints</summary>\n",
    "Use first A. 1.1 from notebook 1 with the definition\n",
    "\n",
    "$$\n",
    "\\omega_{\\text{d}} = \\sqrt{\\frac{e^2 N_e}{m \\epsilon_0 L^3}}\n",
    "$$\n",
    "\n",
    "for $N_e=1$ (single particle) and bring the purely photonic part into diagonal form. Next make the ansatz\n",
    "\n",
    "$$\n",
    "\\left|\\Psi\\right\\rangle = \\sum_{\\tilde{n}=0}^{\\infty} \\Psi(l,\\tilde{n}) \\; \\left|k_l\\right\\rangle \\otimes \\left|\\tilde{n}\\right\\rangle,\n",
    "$$\n",
    "\n",
    "with $\\left\\langle x\\right|k_l\\rangle = \\psi_l(x)$ from A. 2.2 (notebook 2) and show that the Hamiltonian for the photonic part then takes a form similar to Q. 1.2 (notebook 1). Use then A. 1.2 (notebook 1) to find the full solution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A. 3.1\n",
    "\n",
    "Following the hint we find first\n",
    "\n",
    "$$\n",
    "-\\frac{\\hbar^2}{2m}\\partial_x^2 - \\frac{\\mathrm{i} \\hbar \\partial_x}{m} \\frac{|e|}{c} \\hat{A} + \\hbar \\tilde{\\omega}\\left( \\hat{b}^\\dagger \\hat{b} + \\frac{1}{2}\\right),\n",
    "$$\n",
    "\n",
    "with $\\hat{A} = \\sqrt{\\frac{\\hbar c^2}{\\epsilon_0 L^3}} \\frac{1}{\\sqrt{2 \\omega}} (\\hat{b} + \\hat{b}^\\dagger)$ in the new photon coordinates. Making then the ansatz $\\left|\\Psi\\right\\rangle = \\sum_{\\tilde{n}} \\Psi(l,\\tilde{n}) \\; \\left|k_l\\right\\rangle \\otimes \\left|\\tilde{n}\\right\\rangle$ we find again a purely photonic problem of the form\n",
    "\n",
    "$$\n",
    "-\\frac{\\hbar^2 k_l^2}{2m} + \\underbrace{\\frac{\\hbar k_l}{m} \\frac{|e|}{c} \\hat{A}}_{= \\frac{g}{\\sqrt{2 \\tilde{\\omega}}} \\left(\\hat{b} + \\hat{b}^\\dagger \\right)} + \\; \\hbar \\tilde{\\omega}\\left( \\hat{b}^\\dagger \\hat{b} + \\frac{1}{2}\\right),\n",
    "$$\n",
    "\n",
    "where \n",
    "\n",
    "$$\n",
    "g = \\frac{\\hbar k_l}{m} \\sqrt{\\frac{\\hbar e^2}{\\epsilon_0 L^3}} = \\frac{\\hbar^{3/2}k_l}{m^{1/2}} \\omega_{\\text{d}}.\n",
    "$$\n",
    "\n",
    "We thus find in terms of the shifted raising/lowering operators $\\hat{c}^\\dagger$ and $\\hat{c}$\n",
    "\n",
    "$$\n",
    "E_{l,m} = \\frac{\\hbar^2 k_l^2}{2 m} - \\gamma \\frac{(\\hbar k_l)^2}{2 m} + \\hbar \\tilde{\\omega}\\left(m +\\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "with \n",
    "\n",
    "$$\n",
    " \\gamma =   \\frac{\\omega_{\\text{d}}^2}{\\tilde{\\omega}^2} = \\frac{\\omega_{\\text{d}}^2}{\\omega^2 + \\omega_{\\text{d}}^{2}}\n",
    "$$\n",
    "\n",
    "and the eigenfunctions are\n",
    "\n",
    "$$\n",
    "\\left|\\Psi_{l,m}\\right\\rangle = \\left|k_l\\right\\rangle \\otimes \\left|m\\right\\rangle,\n",
    "$$\n",
    "\n",
    "where the $\\left|m\\right\\rangle$ are renormalized and shifted quantum harmonic oscillator eigenstates."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
